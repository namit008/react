import React,{Component} from 'react';
import {Media} from 'reactstrap'
class Menu extends Component{

    constructor(props){
        super(props);
        this.state={
            dishes:[
                {
                    id:0,
                    name:"User 1",
                    image:"assets/img/logo192.png",
                    description:" disc1"
                },
                {
                    id:1,
                    name:"User 2",
                    image:"assets/img/logo192.png",
                    description:" disc 2"
                },
                {
                    id:2,
                    name:"User 3",
                    image:"assets/img/logo192.png",
                    description:"disc 4"
                },
                {
                    id:3,
                    name:" User 4",
                    image:"assets/img/logo192.png",
                    description:"disc 4"
                },
            ]

        }

    }
    

    render() {

        const Menu= this.state.dishes.map((dish)=>{
            return (
                <div key={dish.id} className="col-12 mt-5">
                    <Media tag="li">
                        <Media left middle>
                            <Media object src={dish.image} alt={dish.name}/>

                        </Media>
                        <Media body className="ml=5">
                            <Media heading>{dish.name}</Media>
                            <p>{dish.description}</p>
                        </Media>
                    </Media>
                </div>
            );
        });
        return (
            <div className="container">
                <div className="row">
                    <Media list>
                        {Menu}
                    </Media>
                </div>
            </div>

        );

    }

}


export default Menu;